package bazarbazaraps.hardbassbuttonshare

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.SoundPool
import android.net.Uri
import com.startapp.android.publish.adsCommon.StartAppAd
import com.startapp.android.publish.adsCommon.StartAppSDK


class MainActivity : AppCompatActivity() {

    private lateinit var mp: MediaPlayer


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        StartAppSDK.init(this, "205783796", true);





        fun share_audio( file_path: String ){
            val audio =  Uri.parse(file_path)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "audio/mpeg"
            shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            shareIntent.putExtra(Intent.EXTRA_STREAM, audio);

            startActivity(Intent.createChooser(shareIntent, "HardBass share..."))

        }
        //
        //mp = MediaPlayer.create(this, R.raw.)
        val sp = SoundPool(5, AudioManager.STREAM_MUSIC, 0)
        val soundId = sp.load(this, R.raw.hardb, 1) // in 2nd param u have to pass your desire ringtone
        toggleButton.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                // The toggle is enabled/checked
               // mp.start()
                //mp.isLooping = true
                toggleButton.setBackgroundResource(R.drawable.buttonpressed)

                sp.play(soundId, 1.0f, 1.0f, 0, -1, 1.0f)
                StartAppAd.showAd(this)
            } else {
                // The toggle is disabled
                sp.autoPause()
               // mp.pause()
                StartAppAd.showAd(this)
                toggleButton.setBackgroundResource(R.drawable.buttonstop)
            }

        }

        button.setOnClickListener{
            share_audio( "android.resource://bazarbazaraps.hardbassbutton/raw/hardb" )
            StartAppAd.showAd(this)
        }
    }


    override fun onBackPressed() {
        StartAppAd.onBackPressed(this);
        super.onBackPressed();
    }

}
